﻿using ImagesAPI.Business;
using ImagesAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImagesAPI.Controllers
{
    public class ImagesController : ApiController
    {
        private readonly IImagesBusiness imagesBusiness;

        public ImagesController()
        {
            //This should be initialized by Inversion of control. (Dependency injection)
            imagesBusiness = new ImagesBusiness();
        }

        [HttpGet]
        public IHttpActionResult Get([FromUri] Domain.QueryStringParameters parameters )
        {
            return Ok(imagesBusiness.GetImages(parameters));
        }

        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var image = imagesBusiness.GetImage(id);
            if (image != null)
                return Ok(image);
            else
                return NotFound();
        }

    }
}