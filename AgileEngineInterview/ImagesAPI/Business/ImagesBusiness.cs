﻿using ImagesApi.Domain;
using ImagesAPI.Domain;
using ImagesAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImagesAPI.Business
{
    public class ImagesBusiness : IImagesBusiness
    {
        private readonly List<Image> dbImages;
        private const short PAGE_COUNT = 10;

        public ImagesBusiness()
        {
            dbImages = new List<Image>();
            dbImages.Add(new Image() { id = "3434f531d67bf89ad001", author = "Palatable Drunk 1", camera = "Canon 1301", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0001.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0001.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad002", author = "Palatable Drunk 2", camera = "Canon 1302", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0002.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0002.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad003", author = "Palatable Drunk 3", camera = "Canon 1303", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0003.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0003.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad004", author = "Palatable Drunk 4", camera = "Canon 1304", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0004.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0004.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad005", author = "Palatable Drunk 5", camera = "Canon 1305", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0005.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0005.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad006", author = "Palatable Drunk 6", camera = "Canon 1306", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0006.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0006.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad007", author = "Palatable Drunk 7", camera = "Canon 1307", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0007.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0007.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad008", author = "Palatable Drunk 8", camera = "Canon 1308", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0008.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0008.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad009", author = "Palatable Drunk 9", camera = "Canon 1309", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0009.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0009.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad010", author = "Palatable Drunk 10", camera = "Canon 1310", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0010.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0010.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad011", author = "Palatable Drunk 11", camera = "Canon 1311", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0011.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0011.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad012", author = "Palatable Drunk 12", camera = "Canon 1312", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0012.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0012.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad013", author = "Palatable Drunk 13", camera = "Canon 1313", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0013.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0013.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad014", author = "Palatable Drunk 14", camera = "Canon 1314", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0014.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0014.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad015", author = "Palatable Drunk 15", camera = "Canon 1315", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0015.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0015.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad016", author = "Palatable Drunk 16", camera = "Canon 1316", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0016.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0016.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad017", author = "Palatable Drunk 17", camera = "Canon 1317", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0017.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0017.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad018", author = "Palatable Drunk 18", camera = "Canon 1318", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0018.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0018.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad019", author = "Palatable Drunk 19", camera = "Canon 1319", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0019.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0019.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad020", author = "Palatable Drunk 20", camera = "Canon 1320", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0020.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0020.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad021", author = "Palatable Drunk 21", camera = "Canon 1321", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0021.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0021.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad022", author = "Palatable Drunk 22", camera = "Canon 1322", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0022.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0022.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad023", author = "Palatable Drunk 23", camera = "Canon 1323", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0023.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0023.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad024", author = "Palatable Drunk 24", camera = "Canon 1324", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0024.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0024.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad025", author = "Palatable Drunk 25", camera = "Canon 1325", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0025.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0025.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad026", author = "Palatable Drunk 26", camera = "Canon 1326", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0026.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0026.jpg" });
            dbImages.Add(new Image() { id = "3434f531d67bf89ad027", author = "Palatable Drunk 27", camera = "Canon 1327", cropped_picture = "http://interview.agileengine.com/pictures/cropped/0027.jpg", full_picture = "http://interview.agileengine.com/pictures/full_size/0027.jpg" });
        }

        public PicturesPage GetImages(QueryStringParameters parameters)
        {
            PicturesPage picturePage = new PicturesPage() 
            { 
                pictures = new List<Picture>(),
                pageCount = parameters.limit == null ? PAGE_COUNT : parameters.limit.Value ,
                page = parameters.page
            };

            var query = dbImages;

            if (!string.IsNullOrWhiteSpace(parameters.author)) query = query.Where(image => image.author.Contains(parameters.author)).ToList();

            if (!string.IsNullOrWhiteSpace(parameters.camera)) query = query.Where(image => image.camera.Contains(parameters.camera)).ToList();

            if (!string.IsNullOrWhiteSpace(parameters.pictureName)) query = query.Where(image => image.full_picture.Contains(parameters.pictureName)).ToList();

            picturePage.pictures = query.Skip(picturePage.pageCount * (picturePage.page - 1))
                                        .Take(picturePage.pageCount)
                                        .Select(image => new Picture() { id = image.id, cropped_picture = image.cropped_picture }).AsEnumerable();

            picturePage.hasMore = picturePage.pictures.ToList().Count > 0;

            return picturePage;
        }

        public Image GetImage(string id)
        {
            return dbImages.Find(image => image.id == id);
        }
    }
}