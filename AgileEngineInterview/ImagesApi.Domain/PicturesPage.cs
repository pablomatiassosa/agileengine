﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImagesAPI.Domain
{
    public class PicturesPage
    { 
        public IEnumerable<Picture> pictures { get; set; }
        public int page { get; set; }
        public int pageCount { get; set; }
        public bool hasMore { get; set; }
    }
}
