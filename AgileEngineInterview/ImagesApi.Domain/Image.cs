﻿using ImagesAPI.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImagesApi.Domain
{
    public class Image : Picture
    {
        public Image() : base()
        {

        }

        public string author { get; set; }

        public string camera { get; set; }

        public string full_picture { get; set; }
    }
}
