﻿using ImagesApi.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImagesAPI.Domain.Interfaces
{
    public interface IImagesBusiness
    {
        PicturesPage GetImages(QueryStringParameters parameters);

        Image GetImage(string id);
    }
}
