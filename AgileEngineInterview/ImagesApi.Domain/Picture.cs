﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImagesAPI.Domain
{
    public class Picture
    {
        public string id { get; set; }

        public string cropped_picture { get; set; }
    }
}
