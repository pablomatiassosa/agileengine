﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImagesAPI.Domain
{ 
    public class QueryStringParameters
    {
        public QueryStringParameters()
        {
            this.page = 1;
        }

        public int page { get; set; }

        public int? limit { get; set; }

        public string author { get; set; }

        public string camera { get; set; }

        public string pictureName { get; set; }
    }
}